A Pen created at CodePen.io. You can find this one at http://codepen.io/weisk/pen/qOmydQ.

 A fullscreen landing cover that shrinks into a header on scroll. Uses, once again, skrollr library.

Forked from [Ovi Ovocný](http://codepen.io/Ovoce/)'s Pen [Simple landing page cover](http://codepen.io/Ovoce/pen/jucIg/).