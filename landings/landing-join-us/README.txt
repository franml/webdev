A Pen created at CodePen.io. You can find this one at http://codepen.io/weisk/pen/rOGByN.

 Hey guys !

I was inspired on sunday evening, and I would like to create a kind of landing page, with one or two colors max.

So, I created this fake landing page for fun, you can find the coded version here : lukyvj.github.io/landing and don't forget to check the attached files. 
Btw, the GIF is a bit laggy, so if you want to see the animation, you should click the link

As usual, feel free to like, share and feedback :)

Have a nice day guys !

Forked from [LukyVJ](http://codepen.io/LukyVj/)'s Pen [Leaf Stream](http://codepen.io/LukyVj/pen/FyIhx/).