var gulp = require('gulp');
var gutil = require('gulp-util');
var debug = require('gulp-debug');
var seq = require('gulp-sequence');
var size = require('gulp-size');
var browserSync = require('browser-sync');
var historyApi = require('connect-history-api-fallback');

var jade = require('gulp-jade');
var stylus = require('gulp-stylus');

var loggerFn = function(e){
  gutil.log(e);
};

gulp.task('assets', function(){
  gulp.src('././app/assets/**/*')
    .pipe(gulp.dest('./build/assets/'))
    .pipe(browserSync.stream())
    .on('error', loggerFn);
});

gulp.task('styles', function(){
  gulp.src('./app/app.stylus')
    .pipe(stylus())
    .pipe(gulp.dest('./build/'))
    .pipe(browserSync.stream())
    .on('error', loggerFn);
});

gulp.task('scripts', function(){
  gulp.src('./app/app.js')
    .pipe(gulp.dest('./build/'))
    .pipe(browserSync.stream())
    .on('error', loggerFn);
});

gulp.task('index', function(){
  gulp.src('./app/index.jade')
    .pipe(jade({pretty: true}))
    .pipe(gulp.dest('./build/'))
    .pipe(browserSync.stream())
    .on('error', loggerFn);
});

gulp.task('report', function(){
  dirsize = size({showFiles: true});

  gulp.src('./build/*')
    .pipe(dirsize);
});

gulp.task('server', function(){
  browserSync({
    open: false,
    port: process.env.PORT || 9000,
    server: {
      baseDir: "./build",
      middleware: [ historyApi() ]
    }
  });
});

gulp.task('watch', function(){
  gulp.watch("./app/**/*.js", ['scripts']);
  gulp.watch("./app/**/*.jade", ['index']);
  gulp.watch("./app/**/*.stylus", ['styles']);
  gulp.watch("./app/assets/**/*", ['assets']);
});

gulp.task('build', function(cb){
  seq(['index', 'styles', 'scripts', 'assets'], 'report', cb);
});

gulp.task('serve', function(cb){
  seq(['server', 'watch'], cb);
});

gulp.task('default', function(cb){
  seq('build', 'serve', cb);
});
