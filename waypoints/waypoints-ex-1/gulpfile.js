var gulp = require('gulp');
var gutil = require('gulp-util');
var debug = require('gulp-debug');
var seq = require('gulp-sequence');
var size = require('gulp-size');
var browserSync = require('browser-sync');
var concat = require('gulp-concat');
var autoprefixer = require('gulp-autoprefixer');
var jade = require('gulp-jade');
var sass = require('gulp-sass');

var loggerFn = function(e){
  gutil.log(e);
};

gulp.task('assets', function(){
  gulp.src('./src/assets/**/*')
    .pipe(gulp.dest('./build/assets/'))
    .pipe(browserSync.stream())
    .on('error', loggerFn);
});

gulp.task('styles', function(){
  gulp.src('./src/**/*.{scss,sass}')
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(concat('styles.css'))
    .pipe(gulp.dest('./build/'))
    .pipe(browserSync.stream())
    .on('error', loggerFn);
});

gulp.task('scripts', function(){
  gulp.src('./src/**/*.js')
    .pipe(concat('scripts.js'))
    .pipe(gulp.dest('./build/'))
    .pipe(browserSync.stream())
    .on('error', loggerFn);
});

gulp.task('index', function(){
  gulp.src('./src/index.jade')
    .pipe(jade({pretty: true}))
    .pipe(gulp.dest('./build/'))
    .pipe(browserSync.stream())
    .on('error', loggerFn);
});

gulp.task('report', function(){
  dirsize = size({showFiles: true});

  gulp.src('./build/*')
    .pipe(dirsize);
});

gulp.task('server', function(){
  browserSync({
    open: false,
    port: process.env.PORT || 9000,
    server: {
      baseDir: "./build"
    }
  });
});

gulp.task('watch', function(){
  gulp.watch("./src/**/*.js", ['scripts']);
  gulp.watch("./src/**/*.jade", ['index']);
  gulp.watch("./src/**/*.{scss,sass}", ['styles']);
  gulp.watch("./src/assets/**/*", ['assets']);
});

gulp.task('build', function(cb){
  seq(['index', 'styles', 'scripts', 'assets'], 'report', cb);
});

gulp.task('serve', function(cb){
  seq(['server', 'watch'], cb);
});

gulp.task('default', function(cb){
  seq('build', 'serve', cb);
});
