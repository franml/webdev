A Pen created at CodePen.io. You can find this one at http://codepen.io/weisk/pen/epWRxb.

 Example that shows ability to process appearance of slide blocks in some landing page given by Waypoint plugin. 

Forked from [Alexander Glushchenko](http://codepen.io/somenumboola/)'s Pen [Simple "Waypoint" plugin example.](http://codepen.io/somenumboola/pen/yyZZjw/).