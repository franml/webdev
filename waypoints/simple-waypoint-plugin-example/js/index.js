$(document).ready(function(){
  $('#slides').on(
    'appearance', 
    '.slide', 
    function(e){
    	if (e.direction == 'down') {
      	$(this).addClass('active');  
    	} else {
        $(this).removeClass('active'); 
    	}
  	}
  );
  
  $('#slides > .slide').waypoint({
    handler : function(direction) {
    	var event = new $.Event('appearance');
      event.direction = direction;
      $(this).trigger(event);
    },
    offset : '1%'
  })
})