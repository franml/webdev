#Web Dev Projects

```
#!console

$ tree -L 2

.
├── README.md
├── datatables
│   └── material-sortable-datatable
├── landings
│   ├── a-functional-landing-page
│   ├── dat-product-landing-page
│   ├── landing-join-us
│   ├── landing-page
│   ├── project-landing
│   ├── project-landing-2
│   └── project-landing-3
├── layouts
│   ├── 1-discover-backpacker
│   ├── blog-template
│   ├── freebie-interactive-flat-design
│   ├── make-your-blog-askwithloud
│   └── sticky-widget-askwithloud
├── parallaxes
│   ├── davegamache
│   ├── onmousemove-parallax
│   ├── parallax-scroll
│   └── parallax-world-of-ugg
├── searchboxes
│   ├── ab-search-engine-v2
│   ├── expanding-search-bar
│   ├── material-design-search-bar
│   ├── search-box-practice
│   └── search-box-practice-2
└── waypoints
    ├── simple-waypoint-plugin-example
    └── waypoints-ex-1
```