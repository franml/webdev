A Pen created at CodePen.io. You can find this one at http://codepen.io/weisk/pen/MaXXNv.

 This is a Material Design Search bar that i am working on for <a href="https://storm.it">Storm Search Engine</a>. Inspired by <a href="http://google.com/design/spec">Google Material Design</a>

Forked from [Matt Litherland](http://codepen.io/mattsince87/)'s Pen [Material Design: Search Bar](http://codepen.io/mattsince87/pen/XbMzXM/).