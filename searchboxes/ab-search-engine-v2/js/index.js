var $searchbox = $(".searchbox"),
    $tabs = $(".searchbox-tab", $searchbox),
    $tabsItems = $("li > a", $tabs),
    $panels = $(".searchbox-panel", $searchbox),
    $transac = $(".searchbox-row-transac input", $searchbox),
    $type = $(".searchbox-select-type select", $searchbox),
    $activity = $(".searchbox-select-activity select", $searchbox),
    $transacFilter = $(".searchbox-row-transac-filter input", $searchbox),
    $transacFilterRow = $(".searchbox-row-transac-filter", $searchbox),
    $subactivityRow = $(".searchbox-row-subactivity", $searchbox),
    $form = $("form", $searchbox);

$("input", $searchbox).placeholder();

$tabsItems.on("click", function() {
  var target = $(this).data("target");
  $tabsItems.removeClass("active");
  $(this).addClass("active");
  $panels.hide();
  $("#"+target).show();
  return false;
});

$transac.on("change", function() {
  if ($(this).val() === "1" && $transacFilterRow.is(":visible")) {
    $transacFilterRow.slideUp("fast");
    $("input", $transacFilterRow).prop("disabled", true);
  }
  if ($transac.filter(":checked").val() === "0" && $type.val() === "4") {
    $transacFilterRow.slideDown("fast");
    $("input", $transacFilterRow).prop("disabled", false);
  }
});

$type.on("change", function() {
  if ($transac.filter(":checked").val() === "0" && $(this).val() === "4") {
    $transacFilterRow.slideDown("fast");
    $("input", $transacFilterRow).prop("disabled", false);
  } else {
    $transacFilterRow.slideUp("fast");
    $("input", $transacFilterRow).prop("disabled", true);
  }
}); 

$transacFilter.on("change", function() {
  var nbVal = "2";
  var nb0 = $transacFilter.filter('[name="nb_0"]').is(':checked');
  var nb1 = $transacFilter.filter('[name="nb_1"]').is(':checked');
  if ( nb0 && !nb1) {
    nbVal = "0";
  }
  if ( !nb0 && nb1 ) {
    nbVal = "1";
  }
  $("input[type='hidden']", $transacFilterRow).val(nbVal);
});

$activity.on("change", function() {
  if ($(this).val() !== "0") {
    $subactivityRow.slideDown("fast");
  } else {
    $subactivityRow.slideUp("fast");
  }
});

$form.on("submit", function() {
  var queryString = $(this).formSerialize();
  console.log(queryString);
  return false;
});