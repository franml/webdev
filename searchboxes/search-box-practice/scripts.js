$(function(){

  $("#typed").typed({
    strings: [
      "^2000 <span class='code'>-anchor:example*</span> ^500 links with this text in anchor",
      "^500 <span class='code'>-statuscode:404</span> ^500 links pointing to dead pages",
      "^500 <span class='code'>-domain:example.com</span> ^500 links pointing to domain",
      "^500 links that contain <span class='code'>-anchor:example*</span> ^500 and return <span class='code'>-statuscode:200</span> ^500 on <span class='code'>-domain:example.com</span>"
    ],
    typeSpeed: 0,
    startDelay: 0,
    backDelay: 3000,
    loop: true,
  });

});
