$(function(){

  $.fn.destroyTyped = function(){
    return this.each(function(){
      clearInterval($(this).data('typed').timeout);
      $(this).removeData('typed');
      $(this).siblings('.typed-cursor').remove();
    });
  };

  $(".typed-input").typed({
    strings: [
      "^2000 links with text in anchor <span class='code'>-anchor:example*</span>",
      "^500 links pointing to pages with status <span class='code'>-statuscode:404</span>",
      "^500 links pointing to <span class='code'>-domain:example.com</span>",
      // "^500 links that contain <span class='code'>-anchor:example*</span> ^500 and return <span class='code'>-statuscode:200</span> ^500 on <span class='code'>-domain:example.com</span>"
    ],
    typeSpeed: 0,
    startDelay: 0,
    backDelay: 3000,
    loop: true,
  });

});
