A Pen created at CodePen.io. You can find this one at http://codepen.io/weisk/pen/BodMVG.

 The materialize framework is great but has pretty crappy tables, I made an alternative that can be turned into a sortable datatable with some JS following the Material guidelines as closely a possible

https://www.google.com/design/spec/components/data-tables.html

Forked from [Azamat Mukhiddinov](http://codepen.io/azamatms/)'s Pen [Material Sortable Datatable](http://codepen.io/azamatms/pen/ZGwOMM/).