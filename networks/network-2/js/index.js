var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');

'floor|random|round|abs|sqrt|PI|atan2|sin|cos|pow|max|min'
  .split('|')
  .forEach(function(p) { window[p] = Math[p]; });

var TAU = PI*2;

function r(n) { return random()*n; }
function rrng(lo, hi) { return lo + r(hi-lo); }
function rint(lo, hi) { return lo + floor(r(hi - lo + 1)); }
function choose() { return arguments[rint(0, arguments.length-1)]; }

/*---------------------------------------------------------------------------*/

var W, H, frame, t0, time;
var DPR = devicePixelRatio;

function dpr(n) { return n * DPR; }

function resize() {
  var w = innerWidth;
  var h = innerHeight;
  
  canvas.style.width = w+'px';
  canvas.style.height = h+'px';
  
  W = canvas.width = w * DPR;
  H = canvas.height = h * DPR;
}

function loop(t) {
  frame = requestAnimationFrame(loop);
  draw();
  time++;
}

function pause() {
  cancelAnimationFrame(frame);
  frame = null;
}

function play() {
  frame = frame || requestAnimationFrame(loop);
}

function reset() {
  cancelAnimationFrame(frame);
  resize();
  ctx.clearRect(0, 0, W, H);
  init();
  time = 0;
  frame = requestAnimationFrame(loop);
}

/*---------------------------------------------------------------------------*/

function Node(x, y) {
  this.x = x;
  this.y = y;
  this.friends = [];
  this.isActive = false;
  this.activatedAt = 0;
}

Node.prototype.activate = function(t) {
  this.isActive = true;
  this.activatedAt = t;
}

Node.prototype.update = function(t, p) {
  if (!this.isActive) return;
  if (t - this.activatedAt >= 3) {
    this.isActive = false;
    this.friends.forEach(function(node) {
      if (random() < p) node.activate(t);
    });
  }
};

Node.prototype.makeFriends = function(others) {
  var node = this;
  others.forEach(function(other) {
    if (other === node) return;
    //if (other.friends.indexOf(node) >= 0) return;
    if (node.tooFarFrom(other)) return;
    if (random() < 0.2) node.friends.push(other);
  });
};

Node.prototype.tooFarFrom = function(other) {
  return this.d2(other.x, other.y) >= FAR2;
};

Node.prototype.d2 = function(x, y) {
  var dx = this.x - x;
  var dy = this.y - y;
  return dx*dx + dy*dy;
};

function Arr(n) {
  this.arr = new Array(n);
  this.len = 0;
}

Arr.prototype.push = function(x) { this.arr[this.len++] = x; };
Arr.prototype.concat = function(a) { 
  for (var i = 0; i < a.length; i++) this.push(a[i]);
};
Arr.prototype.clear = function() { this.len = 0; };
Arr.prototype.each = function(cb) {
  for (var i = 0; i < this.len; i++) cb(this.arr[i]);
};

/*---------------------------------------------------------------------------*/

var FAR2 = dpr(30*30);
var N = 10000;
var P;
var ons;
var nodes = [];

function init() {
  for (var i = 0; i < N; i++) {
    nodes[i] = new Node(rint(100, W-100), rint(100, H-100));
  }
  nodes.forEach(function(node) { node.makeFriends(nodes); });
  
  var count = nodes.reduce(function(sum, node) {
    return sum + node.friends.length;
  }, 0);
  
  ons = new Arr(count);
}

function draw() {
  var i;
  P *= 0.98;
  for (i = 0; i < N; i++) nodes[i].update(time, P);
  ons.clear();
  
  for (i = 0; i < N; i++) {
    var node = nodes[i];
    if (!node.isActive) continue;
    node.friends.forEach(function(friend) {
      ons.push({
        from: node,
        to: friend
      });
    });
  }
  
  drawLines(ons, 'rgba(34, 49, 63, 0.02)', dpr(0.5));
  
  if (ons.len === 0) pause();
}

function drawLines(which, color, width) {
  ctx.strokeStyle = color;
  ctx.lineWidth = width;
  ctx.beginPath();
  which.each(function(spec) {
    ctx.moveTo(spec.from.x, spec.from.y);
    ctx.lineTo(spec.to.x, spec.to.y);
  });
  ctx.closePath();
  ctx.stroke();
}

function findNearestNode(x, y) {
  var d2 = Infinity;
  var nearest = null;
  for (var i = 0; i < N; i++) {
    var noded2 = nodes[i].d2(x, y);
    if (noded2 < d2) {
      d2 = noded2;
      nearest = nodes[i];
    }
  }
  return nearest;
}

function kickoff(x, y) {
  for (var i = 0; i < N; i++) nodes[i].isActive = false;
  var node = findNearestNode(x, y);
  P = 1;
  ctx.clearRect(0, 0, W, H);
  node.activate(time);
  play(); 
}

/*---------------------------------------------------------------------------*/

document.ondblclick = function() {
  kickoff(W/2, H/2);
};

document.onclick = function(e) {
  var x = dpr(e.pageX);
  var y = dpr(e.pageY);
  kickoff(x, y);
};

reset();
document.getElementById('loader').style.display = 'none';
kickoff(W/2, H/2);