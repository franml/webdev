A Pen created at CodePen.io. You can find this one at http://codepen.io/weisk/pen/dYRXmB.

 This is a quick example of using default bootstrap components and by adding a very small amount of CSS and JS you can have a very useful fixed to navbar addaption :)

Forked from [Craig Watson](http://codepen.io/Craig-Watson/)'s Pen [Bootstrap Navbar Hide on Scroll Down, Show on Scroll Up On Mobiles Only!](http://codepen.io/Craig-Watson/pen/zGbNRL/).