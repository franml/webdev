;(function ($, window, undefined) {
	$(function() {

		$("a, input[type=submit]").on("click", function(e) { e.preventDefault(); });

		$("nav.main ul li").on("click", function() {
			var $this = $(this),
				$links = $("nav.main ul li");

			$links.removeClass("selected");
			$this.addClass("selected");
		});

		var icons = {
			header: "icon-chat-status-close",
			activeHeader: "icon-chat-status-open"
		};

		$("nav.chat .online, nav.chat .offline").accordion({
			collapsible: true,
			icons: icons
		});
	});
})(jQuery, this);