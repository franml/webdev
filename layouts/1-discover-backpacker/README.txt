A Pen created at CodePen.io. You can find this one at http://codepen.io/weisk/pen/KdeeOa.

 This is a design I took from: 
https://dribbble.com/shots/1149909-1-Discover-Backpacker/attachments/148605

Just for fun and to stay sharp. 
**FOR BETTER RESULTS PLEASE VIEW IN CHROME and FULLSCREEN**

Still a work in progress though.

Forked from [Kev Rousseau](http://codepen.io/kevrousso/)'s Pen [1-Discover-Backpacker](http://codepen.io/kevrousso/pen/hntgi/).