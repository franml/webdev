A Pen created at CodePen.io. You can find this one at http://codepen.io/weisk/pen/OyoyVK.

 Inspired by: 
http://graphicburger.com/flat-design-ui-components/

Line-chart and donut-chart made by @Kseso: http://codepen.io/Kseso/pen/phiyL

Forked from [Javier Latorre López-Villalta](http://codepen.io/jlalovi/)'s Pen [Freebie Interactive Flat Design UI / Only HTML5 & CSS3](http://codepen.io/jlalovi/pen/bIyAr/).