A Pen created at CodePen.io. You can find this one at http://codepen.io/weisk/pen/zvLjGb.

 The actual function of this widget is that "The sticky widget is useful for my friends who wanted a widget on their blog in order to float follows when the page rolled down and will return to its original position when page rolled up".

Forked from [Prince](http://codepen.io/Askwithloud/)'s Pen [Make Your Blog Widgets And Navbar Sticky By Askwithloud](http://codepen.io/Askwithloud/pen/LpGPOj/).